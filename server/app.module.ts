import { NextModule } from './modules/next'

import { Module } from '@nestjs/common'

@Module({
  imports: [NextModule],
  providers: [NextModule],
})
export class AppModule {}
