import { NextService } from './next.service'
import { SsrCacheInterceptor } from './ssr-cache.interceptor'

import {
  Controller,
  Get,
  Inject,
  Req,
  Res,
  UseInterceptors,
} from '@nestjs/common'

@Controller()
export class NextController {
  constructor(@Inject(NextService) private readonly next: NextService) {}

  @Get('*')
  @UseInterceptors(SsrCacheInterceptor)
  render(
    @Req() req: App.ServerRequest,
    @Res() res: App.ServerResponse,
  ): Promise<void> {
    return this.next.app.getRequestHandler()(req, res)
  }
}
