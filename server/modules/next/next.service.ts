import cacheableResponse from 'cacheable-response'
import next from 'next'

import { Injectable, OnModuleInit } from '@nestjs/common'

@Injectable()
export class NextService implements OnModuleInit {
  public readonly app = next({
    dev: process.env.NODE_ENV === 'development',
    dir: process.cwd(),
    customServer: true,
  })

  public ssrCache = cacheableResponse({
    ttl: 1000 * 60 * 60, // 1hour
    // @ts-ignore
    getKey: ({
      req,
      res,
    }: {
      req: App.ServerRequest
      res: App.ServerResponse
    }) => `${req.url}`,
    get: async ({
      req,
      res,
    }: {
      req: App.ServerRequest
      res: App.ServerResponse
    }) => {
      const data = await this.app.renderToHTML(req, res, req.path)

      // Add here custom logic for when you do not want to cache the page, for
      // example when the page returns a 404 status code:
      if (res.statusCode === 404) {
        res.end(data)
        return
      }

      return { data }
    },
    send: ({
      data,
      res,
    }: {
      data: string
      res: App.ServerResponse
      req: App.ServerRequest
    }) => res.send(data),
  })

  async onModuleInit() {
    await this.app.prepare()
  }
}
