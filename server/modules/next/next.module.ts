import { NextController } from './next.controller'
import { NextService } from './next.service'
import { SsrCacheInterceptor } from './ssr-cache.interceptor'

import { MiddlewareConsumer, Module, NestModule } from '@nestjs/common'

@Module({
  controllers: [NextController],
  providers: [NextService, SsrCacheInterceptor],
  exports: [NextService],
})
export class NextModule {}
