import { NextService } from './next.service'

import {
  CallHandler,
  ExecutionContext,
  Inject,
  Injectable,
  NestInterceptor,
} from '@nestjs/common'

@Injectable()
export class SsrCacheInterceptor implements NestInterceptor {
  constructor(@Inject(NextService) private readonly next: NextService) {}

  async intercept(context: ExecutionContext, next: CallHandler) {
    const httpHost = context.switchToHttp()

    const [req, res] = [
      httpHost.getRequest<App.ServerRequest>(),
      httpHost.getResponse<App.ServerResponse>(),
    ]

    if (req.method === 'GET') {
      const isPage = await this.next.app.router.pageChecker(req.path)
      const isDynamicPage = this.next.app.router.dynamicRoutes.some(
        (item) => !!item.match(req.path),
      )

      if (isPage || isDynamicPage) {
        await this.next.ssrCache({ req, res })
      }
    }

    return next.handle()
  }
}
