import 'reflect-metadata'

import { AppModule } from './app.module'

import { NestFactory } from '@nestjs/core'
import { NestExpressApplication } from '@nestjs/platform-express'

const port = process.env.PORT || 3000
const isDev = process.env.NODE_ENV === 'development'

async function bootstrap() {
  const server = await NestFactory.create<NestExpressApplication>(AppModule, {
    cors: true,
  })

  server.enableCors()
  server.disable('x-powered-by')

  await server.init()

  await server.listenAsync(port)
  if (isDev) {
    console.log(`App ready on http://localhost:${port}`)
  }
}

bootstrap()
