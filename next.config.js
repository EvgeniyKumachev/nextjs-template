const path = require('path')
const fs = require('fs')
const withPlugins = require('next-compose-plugins')
const TsconfigPathsPlugin = require('tsconfig-paths-webpack-plugin')

const isProduction = process.env.NODE_ENV === 'production'

module.exports = withPlugins([], {
  experimental: {
    workerThreads: true,
    modern: isProduction,
    documentMiddleware: isProduction,
  },
  webpack(config, { isServer }) {
    config.resolve.plugins = [
      new TsconfigPathsPlugin({
        configFile: path.resolve(__dirname, './tsconfig.json'),
      }),
    ]

    return config
  },
})
